#!/bin/bash
export HOSTNAME=routeco.app

export BRANCH=staging
export BRANCH_OR_VERSION=staging

if [[ $BRANCH == "master" ]]; then
	export APP_SLUG=
	export APP_NAME=routes-frontend
	export URL=$HOSTNAME
	export NAMESPACE=production
else
	export APP_SLUG=$BRANCH_OR_VERSION
	export APP_NAME=routes-frontend-$APP_SLUG
	export URL=$APP_SLUG.dev.$HOSTNAME
	export NAMESPACE=staging
fi


kubectl create namespace $NAMESPACE
kubectl apply -n $NAMESPACE -f deploy/issuer.yml
envsubst < deploy/deployment.yml | kubectl apply -n $NAMESPACE -f -
envsubst < deploy/ingress.yml | kubectl apply -n $NAMESPACE -f -

if [[ $NAMESPACE == "production" ]]; then
    envsubst < deploy/ingress-legacy.yml | kubectl apply -n $NAMESPACE -f -
fi
